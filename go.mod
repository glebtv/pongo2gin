module gitlab.com/glebtv/pongo2gin

go 1.13

require (
	github.com/flosch/pongo2 v0.0.0-20190707114632-bbf5a6c351f4
	github.com/gin-gonic/gin v1.4.0
)
